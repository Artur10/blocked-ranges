﻿using System;
using NUnit.Framework;
using BlockedRanges;

namespace BlockedRangesTest
{
    [TestFixture]
    public class ProgramTest
    {
        [Test]
        public void InverDatesRangeTest()
        {
            //Arrange
            DateTime minDate = new DateTime(2015, 1, 1);
            DateTime maxDate = DateTime.Today;
            var testBlockedRanges = new[]
            {
                new []
                {
                    new DateTime ( 2015 , 1 , 1 ),
                    new DateTime ( 2015 , 2 , 4 ),
                },
                new []
                {
                    new DateTime ( 2015 , 2 , 12 ),
                    new DateTime ( 2015 , 4 , 15 ),
                },
                new []
                {
                    new DateTime ( 2015 , 5 , 5 ),
                    new DateTime ( 2015 , 8 , 20 ),
                },
                new []
                {
                    new DateTime ( 2015 , 8 , 20 ),
                    new DateTime ( 2015 , 10 , 5 ),
                },
                new[]
                {
                    DateTime.Today.AddDays(- 1),
                    maxDate,
                }
            };

            var availableRanges = new[]
            {
                new []
                {
                    new DateTime ( 2015 , 2 , 4 ),
                    new DateTime ( 2015 , 2 , 12 ),
                },
                new []
                {
                    new DateTime ( 2015 , 8 , 20 ),
                    new DateTime ( 2015 , 8 , 20 ),
                },
                new []
                {
                    new DateTime ( 2015 , 4 , 15 ),
                    new DateTime ( 2015 , 5 , 5 ),
                },
                new []
                {
                    new DateTime ( 2015 , 10 , 5 ),
                    DateTime.Today.AddDays(- 1)
                }
            };
            //Act
            var resultRanges = Program.InvertDateRanges(availableRanges, minDate, maxDate);
            //Assert
            CollectionAssert.AreEquivalent(resultRanges,testBlockedRanges);
        }
    }
}
