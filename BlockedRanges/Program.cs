﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlockedRanges
{
    public class Program
    {
        static void Main(string[] args)
        {
            DateTime minDate = new DateTime(2015, 1, 1), maxDate = DateTime.Today;
            var availableRanges = new[]
            {
                new []
                {
                    new DateTime ( 2015 , 2 , 4 ),
                    new DateTime ( 2015 , 2 , 12 ),
                },
                new []
                {
                    new DateTime ( 2015 , 8 , 20 ),
                    new DateTime ( 2015 , 8 , 20 ),
                },
                new []
                {
                    new DateTime ( 2015 , 4 , 15 ),
                    new DateTime ( 2015 , 5 , 5 ),
                },
                new []
                {
                    new DateTime ( 2015 , 10 , 5 ),
                    DateTime.Today.AddDays(- 1)
                }
            };
            var blockedRanges = InvertDateRanges(availableRanges, minDate, maxDate);
            foreach (var range in blockedRanges)
            {
                Console.WriteLine("{0:dd.MM.yyyy} - {1:dd.MM.yyyy}", range[0], range[1]);
            }
            Console.ReadKey();
        }
        public static DateTime[][] InvertDateRanges(DateTime[][] inputRanges, DateTime minDate, DateTime maxDate)
        {
            List<DateTime[]> invertRange = new List<DateTime[]>();
            var allDates = GetAllDatesInSorteredOrder(inputRanges, minDate, maxDate);
       
            for(int i = 0; i <= allDates.Count() + 2; i++)
            {
                var range = allDates.Take(2);
                DateTime[] arrayRange = range.ToArray();
                invertRange.Add(arrayRange);
                allDates.RemoveRange(0, 2);
            }
            return invertRange.ToArray();
        }
        public static List<DateTime> GetAllDatesInSorteredOrder(DateTime[][] inputRanges, DateTime minDate, DateTime maxDate)
        {
            List<DateTime> dates = new List<DateTime>();
            dates.Add(minDate);
            dates.Add(maxDate);
            for (int i = 0; i < inputRanges.Length; i++)
            {
                dates.AddRange(inputRanges[i]);
            }
            dates.Sort((a, b) => a.CompareTo(b));
            return dates;
        }
    }
}
